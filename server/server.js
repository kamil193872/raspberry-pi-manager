// @flow
/* eslint-disable strict */
const Express = require('express'),
	app = Express(),
    compression = require('compression'),
    http = require('http').Server( app ),
    socketio_api = require('./server/api/socketio'),
    bodyParser = require('body-parser'),
    //mongoose = require("./server/init/mongoDB"),
    router = require('./server/controllers/mainController'),
    path = require("path");


app.use(compression());
app.use(Express.static(path.join(__dirname, "public"), {dotfiles: 'allow'}));
app.use(bodyParser.json());
socketio_api(http);
app.use(router);


http.listen( 3000, ( err ) => {
    console.log( 'Listen on port 3000' );
    if ( err ) {
        console.log( err );
    }
} );
