const mongoose = require("mongoose");

let userSchema = mongoose.Schema({
    login: String,
    sessionId: String
});

userSchema.methods.speak = function() {
    console.log('Login: ${this.login}, SessionId: ${this.sessionId}');
};

module.exports = mongoose.model("User", userSchema);
