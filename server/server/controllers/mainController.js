const Router = require('express').Router,
    User = require('../model/user'),
    path = require("path");

const routes = Router();

/*routes.get( '/', (req, res) => {
    res.sendFile( path.join(__dirname, "public", "index.html") );
} );

routes.post('/user', (req, res) => {
    let newUser = new User(req.body);
    newUser.save((err, entity) => {
        if(err){
            console.log(err);
        } else {
            entity.speak();
        }

    });
});*/

routes.use(function (req, res) {
    res.sendFile( path.join(__dirname, '../..', 'public', 'index.html') );
})

module.exports = routes;
