const mongoose = require("mongoose"),
    config = require("../config/mongoDBConfig");

mongoose.connect(config.url);

let db = mongoose.connection;

db.on("error", err => {
    console.log("DATABASE ERROR:");
    console.log(err);
});

db.on("open", () => {
    console.log("BD connected...");
});

exports = mongoose;