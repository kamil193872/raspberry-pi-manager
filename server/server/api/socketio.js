const SocketIO = require('socket.io');

module.exports = (http) => {
    const io = SocketIO(http);

    const EVT_CMN_CONNECTION = 'connection',
        EVT_CMN_DISCONNECT = 'disconnect',
        EVT_CMN_COMMAND = 'command',
        EVT_CLNT_COMMAND_RESPONSE = 'client-cmd-response',
        EVT_CLNT_USER_LIST = 'user-list',
        EVT_CLNT_CLIENT_UPDATE = 'client-update';

    const clientNS = io.of( '/client' ),
        userNS = io.of( '/user' );

    /**
     *	@param id = {
     *		@param name
     *		@param socket
     *		@param extensions
     * }
     * */

    /*type ClientExtension = {
        name: string,
        input: string,
        output: string
    }

    type ClientObject = {
        [key: string]: {
            name: string,
            socket: WebSocket,
            extensions: Array<ClientExtension>,
            icon?: string
        }
    }*/
    let clientsTab/*: ClientObject*/ = {},
        usersTab = [];

    let connListUpdate = () => {
        let clients = [];

        for( let id in clientNS.sockets ) {
            if( clientsTab[ id ] ) {
                let { name, icon, extensions, date } = clientsTab[ id ];
/*
                if( "name" in clientsTab[ id ] ) {
                    value.name = clientsTab[ id ].name;
                }
                if( "icon" in clientsTab[ id ] ) {
                    value.icon = clientsTab[ id ].icon;
                }
                if( "extensions" in clientsTab[ id ] ) {
                    value.extensions = clientsTab[ id ].extensions;
                }*/
                clients.push( { id, name, icon, extensions, date } );
            }

        }

        userNS.emit( EVT_CLNT_USER_LIST, { clients, 'users': Object.keys( userNS.sockets ) } );
    };


    io.on( EVT_CMN_CONNECTION, (socket) => {
        let id = socket.id;
        // console.log(`connected - ${id}`);

        socket.on( EVT_CMN_DISCONNECT, () => {
            // console.log(`disconnected - ${id}`);
        } );
    } );

    userNS.on( EVT_CMN_CONNECTION, (socket) => {
        console.log( `USER   - connection - ${socket.id}` );

        let userId = socket.id;
        usersTab[ userId ] = { socket };

        socket.on( EVT_CMN_COMMAND, function(clientId, extName, data) {
            console.log( data );

            let p2pConn = (clientsTab[ clientId ] || {}).socket,
                callerId = this.id;

            if( p2pConn ) {
                p2pConn.emit( EVT_CMN_COMMAND, {callerId, extName, data} );
            }
        } );

        socket.on( EVT_CMN_DISCONNECT, () => {
            console.log( 'USER - disconnected' );

            connListUpdate();
        } );

        connListUpdate();
    } );

    clientNS.on( EVT_CMN_CONNECTION, (socket) => {
        console.log( `CLIENT - connection - ${socket.id}` );

        let id = socket.id;

        clientsTab[ id ] = { socket, date:  Date.now()};
        connListUpdate();

        socket.on( EVT_CLNT_COMMAND_RESPONSE, function( {callerId, destinationId, extName, data} ) {
            console.log( EVT_CLNT_COMMAND_RESPONSE +" "+ data );

            let userSocket = (usersTab[destinationId] || {}).socket;
            if (userSocket) {
                userSocket.emit(EVT_CLNT_COMMAND_RESPONSE, {callerId, extName, data});
            }
        } );

        socket.on( EVT_CLNT_CLIENT_UPDATE, ( data ) => {
            if( !!data && !!data.id && !!clientsTab[ data.id ] ) {
                Object.assign(clientsTab[ data.id ], data);
            }
            connListUpdate();
        } );

        socket.on( EVT_CMN_DISCONNECT, ( xx, yy ) => {
            console.log( 'CLIENT - disconnected' );

            delete clientsTab[ id ];
            connListUpdate();
        } );

    } );

    return io;

}