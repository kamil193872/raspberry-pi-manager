const path = require('path'),
	BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin,
	nodeExternals = require('webpack-node-externals');

module.exports = {
	target: 'web',
    //externals: [nodeExternals({whitelist:["react", "react-dom", "antd/lib", "socket.io-client",/.*node_modules.*/]})],
	entry: "./web_client/compiled/app.js",
	output: {
		path: path.resolve(__dirname, "public", "js"),
		filename: "bundle.js"
	},
	plugins: [
		//new BundleAnalyzerPlugin()
	],
	module:{
		rules: [
		  {
			test: /\.css$/,
			use: [ 'style-loader', 'css-loader' ]
		  }
		]
	}
};