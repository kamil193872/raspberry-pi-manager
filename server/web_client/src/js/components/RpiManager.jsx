import React from 'react'
import List from './List'
import io from '../api/SocketIO'


const EVT_CLNT_CLIENT_UPDATE = 'client-update',
    EVT_CLNT_COMMAND_RESPONSE = 'client-cmd-response',
    EVT_CMN_COMMAND = 'command',
    EVT_CLNT_USER_LIST = 'user-list';

class RpiManager extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            users: [],
            clients: []
        }
    }

    componentDidMount() {
        io.on(EVT_CMN_COMMAND, data=>{
            console.log(`command: ${data}`);
            })
        .on(EVT_CLNT_COMMAND_RESPONSE, ({callerId, extName, data})=>{
            console.log(EVT_CLNT_COMMAND_RESPONSE +" : " + data.toString());
            console.timeEnd(callerId);

            let newClients = Array.from(this.state.clients);

            newClients.forEach(client => {
                if (client.id == callerId) {
                    (client.extensions || []).forEach(ext => {
                        if (ext.name == extName) ext.response = data;
                    })
                }
            })

            this.setState({clients: newClients});
            this.clearResponse(callerId, extName, 3000);
        })
        .on(EVT_CLNT_USER_LIST, data=>{
            //insertListData(data);
            console.log("USER LIST");
            console.log(data);
            this.setState({
                users: data.users,
                clients: data.clients
            });
        })
        .on("connect",()=>{
            console.log(`polaczony, ID: ${io.id}`);
        })
        .on(EVT_CLNT_CLIENT_UPDATE,(data)=>{
            if(!data || !data.id || !data.name) return;
            console.log(EVT_CLNT_CLIENT_UPDATE);
            console.log(data);

        })
    }

    clearResponse(callerId, extName, time) {
        setTimeout(function () {
            let newClients = Array.from(this.state.clients);

            newClients.forEach(client => {
                if (client.id == callerId) {
                    (client.extensions || []).forEach(ext => {
                        if (ext.name == extName) ext.response = "";
                    })
                }
                this.setState({clients: newClients});
            });

            this.setState({clients: newClients});

        }.bind(this), time);
    }

    callExternalFunction(clientId, extName, value) {
        console.time(clientId);
        console.log(`client: ${clientId}, function name: ${extName}, value: ${value}`);
        io.emit(EVT_CMN_COMMAND, clientId, extName, value);
    }
    render() {
        return (
            <div className="row">
                <div className="user-list">
                    <h2>Users</h2>
                    <List type="USERS" items={this.state.users}/>
                </div>
                <div className="device-list">
                    <h2>Devices</h2>
                    <List type="DEVICES" items={this.state.clients} callExternalFunction={this.callExternalFunction}/>
                </div>
            </div>
        )
    }

}

export default RpiManager