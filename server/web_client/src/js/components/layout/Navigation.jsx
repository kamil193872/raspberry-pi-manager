import React from 'react'
import {NavLink} from 'react-router-dom'

class Navigation extends React.Component {
    render() {
        return (
            <div className="navigation-left">
                <div className="navigation-logo-container">
                    <img src="img/layout/rpi_logo_bw_transparent.svg"/>
                    <div className="phrase">
                        <span>Raspberry Pi </span>
                        <span>Manager</span>
                    </div>
                    <hr/>
                    <nav>
{/*                        <NavLink to="/">Main Page</NavLink><br/>
                        <NavLink to="/login">Login</NavLink>*/}
                    </nav>
                </div>
            </div>
        )
    }

}

export default Navigation