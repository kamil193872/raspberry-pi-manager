import React from 'react'

class TopBanner extends React.Component {
    render() {
        return (
            <div className="top-banner text-center">
                <h2>Raspberry Pi manager
                    <img className="logo" src="img/layout/rpi_logo_color_transparent.png"></img>
                </h2>
            </div>
        )
    }

}

export default TopBanner