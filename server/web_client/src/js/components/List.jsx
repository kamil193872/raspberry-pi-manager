import React from "react"
import DeviceListItem from "./DeviceListItem"
import UserListItem from "./UserListItem";

class List extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            items: (props.items || []),
            type: props.type,
            transparentMode: false,
            activeDevice: null
        };
    }

    componentWillReceiveProps(props) {
        this.setState(props);
    }

    selectionChanged(state) {
        this.setState(state);
    }

    render(){
        return(
            <div className="row">
                {this.state.items.map((item)=>{
                    let listItem = null;



                    switch (this.state.type) {
                        case "USERS": {
                            listItem = <UserListItem key={item} data={item}/>;
                            break;
                        }
                        case "DEVICES": {
                            item.transparentMode = this.state.transparentMode;
                            item.active = this.state.activeDevice === item.id;

                            listItem = <DeviceListItem
                                        key={item.id}
                                        data={item}
                                        selectionChanged={(state)=>{this.selectionChanged(state)}}
                                        callExternalFunction={this.props.callExternalFunction}
                                        />;
                            break;
                        }

                    }

                    return listItem;

                })}
            </div>
        )
    }

}
export default List