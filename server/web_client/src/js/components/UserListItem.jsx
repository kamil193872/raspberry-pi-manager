import React from "react"
import Avatar from 'antd/lib/avatar'

import 'antd/lib/avatar/style/css';

class UserListItem extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            id: props.data
        };
    }

    componentWillReceiveProps(props) {
        this.setState(props);
    }

    render(){
        return(
            <div className="user-avatar-container">
                <Avatar icon="user"  size="small"></Avatar>
                <span>{this.state.id}</span>
            </div>
        )
    }

}
export default UserListItem