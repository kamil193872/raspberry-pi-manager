import React from "react"
import Tabs from 'antd/lib/tabs';
import Input from 'antd/lib/input';
import Icon from 'antd/lib/icon';
import Button from 'antd/lib/button';

import moment from 'moment'

import 'antd/lib/tabs/style/css';
import 'antd/lib/input/style/css';
import 'antd/lib/icon/style/css';
import 'antd/lib/button/style/css';

const TabPane = Tabs.TabPane;

function callback(key) {
    console.log(key);
}


const URL_IMG = "https://cdn.iconscout.com/public/images/icon/free/png-48/raspberry-pi-logo-34e5a2f43f2e9f43-48x48.png";

class DeviceListItem extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            id: props.data.id,
            name: props.data.name,
            icon: (props.data.icon || URL_IMG),
            date: props.data.date,
            extensions: (props.data.extensions || []),
            active: false,
            onlineTimerId: null,
            transparentMode: false,
            data: {},
            onlineTime :null,
            response: null
        };
    }

    componentWillUnmount() {
        clearInterval(this.state.onlineTimerId);
        this.setState({onlineTimerId: null});
    }

    componentDidMount() {
        let onlineTimerId = setInterval(this.getOnlineTime.bind(this), 1000);
        this.setState({onlineTimerId});
        this.getOnlineTime(true);
    }

    componentWillReceiveProps(props) {
        this.setState(props.data);
    }

    selectionChanged(isSelected) {
        this.props.selectionChanged({
            activeDevice: (isSelected) ? (this.state.id) : (null),
            transparentMode: isSelected
        });
    }

    callClientFunction(extName) {
        console.log(extName);
        this.props.callExternalFunction(this.state.id, extName, this.state.data);
    }

    onInput(fieldName, value) {
        let data = this.state.data;
        data[fieldName] = value;
        this.setState({data});
    }

    render(){
        let classes = "device-list-item";

        classes = (this.state.active) ? (classes += " device-list-active") : (classes);
        classes = (this.state.transparentMode && !this.state.active) ? (classes += " device-list-transparent") : (classes);

        let extensionBadge = (!this.state.active && this.state.extensions.length)
            ? <p className="extension-badge">
                Extension <span className="badge badge-secondary">{this.state.extensions.length}</span>
              </p>
            : null;

        let online = (this.state.date && !this.state.active)
            ? <span className="device-list-item-timer">{this.state.onlineTime}</span>
            : null;

        let extensions = (this.state.active)
            ? (<Tabs onChange={callback}>
                {this.state.extensions.map(ext =>
                    <TabPane tab={ext.name} key={ext.name}>

                        <span className="device-list-item-desc">{ext.description}</span>

                        <Input.Group>
                            {ext.input.map(field =>
                                <Input type="text"
                                       name={field.name}
                                       key={field.name}
                                       placeholder={field.description}
                                       onChange={(evt)=>{this.onInput(field.name, evt.target.value)}}
                                />
                            )}
                            <Button type="primary" onClick={()=>{this.callClientFunction(ext.name)}}>
                                <Icon type="arrow-right" />
                            </Button>

                            <pre className="device-list-item-response">{ext.response || ""}</pre>

                        </Input.Group>

                    </TabPane>

                )}

                <TabPane tab="Placeholder 1" key="2">Content of Tab Placeholder 1</TabPane>
                <TabPane tab="Placeholder 2" key="3">Content of Tab Placeholder 2</TabPane>
             </Tabs>)
            : null;

        return(
            <div tabIndex="0" className={classes} onFocus={()=>{this.selectionChanged(true)}}>
                <button className="close" onClick={()=>{this.selectionChanged(false)}}>&times;</button>
                <img src={this.state.icon} className="device-list-item-icon"/>
                <span className="device-list-item-name">{this.state.name}</span>
                {!this.state.active ? <hr/> : null}
                    {online}
                    {extensionBadge}
                    {extensions}
            </div>
        )
    }

    getOnlineTime(force) {
        if (force === true || !this.state.active) {
            let then = moment(this.state.date),
                now = moment(Date.now()),
                diff = now.diff(then),
                hours = parseInt(moment.duration(diff).asHours()),
                minutes = parseInt(moment.duration(diff - hours * 3600 * 1000).asMinutes()),
                seconds = parseInt(moment.duration(diff - hours * 3600 * 1000 - minutes * 60 * 1000).asSeconds());
            let onlineTime = `Online ${hours}h : ${minutes}min : ${seconds}s`;

            this.setState({onlineTime});
        }
    }

}
export default DeviceListItem