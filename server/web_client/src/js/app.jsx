import React from 'react'
import ReactDOM from 'react-dom'
import {BrowserRouter, Route, Switch, Redirect} from 'react-router-dom'
import RpiManager from './components/RpiManager'
import TopBanner from './components/layout/TopBanner'
import Navigation from './components/layout/Navigation'

const MainView = () =>
	<React.Fragment>
		<TopBanner/>
		<div className="content-container">
			<Navigation/>
			<div className = "main-container">
				<Switch>
					<Route path="/" exact component={rpiView} />
					<Route path="/login" component={loginView} />
					<Redirect to="/"/>
				</Switch>
			</div>
		</div>
	</React.Fragment>;

const loginView = () =>
	<h2>/login</h2>;

const rpiView = () =>
    <RpiManager/>


let container =
	<BrowserRouter>
		<MainView/>
	</BrowserRouter>


ReactDOM.render(container, document.getElementById('app-container'));