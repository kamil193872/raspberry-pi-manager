import io from 'socket.io-client'

const usersNS = io('/user');

export default usersNS;