'use strict';

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactDom = require('react-dom');

var _reactDom2 = _interopRequireDefault(_reactDom);

var _reactRouterDom = require('react-router-dom');

var _RpiManager = require('./components/RpiManager');

var _RpiManager2 = _interopRequireDefault(_RpiManager);

var _TopBanner = require('./components/layout/TopBanner');

var _TopBanner2 = _interopRequireDefault(_TopBanner);

var _Navigation = require('./components/layout/Navigation');

var _Navigation2 = _interopRequireDefault(_Navigation);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var MainView = function MainView() {
	return _react2.default.createElement(
		_react2.default.Fragment,
		null,
		_react2.default.createElement(_TopBanner2.default, null),
		_react2.default.createElement(
			'div',
			{ className: 'content-container' },
			_react2.default.createElement(_Navigation2.default, null),
			_react2.default.createElement(
				'div',
				{ className: 'main-container' },
				_react2.default.createElement(
					_reactRouterDom.Switch,
					null,
					_react2.default.createElement(_reactRouterDom.Route, { path: '/', exact: true, component: rpiView }),
					_react2.default.createElement(_reactRouterDom.Route, { path: '/login', component: loginView }),
					_react2.default.createElement(_reactRouterDom.Redirect, { to: '/' })
				)
			)
		)
	);
};

var loginView = function loginView() {
	return _react2.default.createElement(
		'h2',
		null,
		'/login'
	);
};

var rpiView = function rpiView() {
	return _react2.default.createElement(_RpiManager2.default, null);
};

var container = _react2.default.createElement(
	_reactRouterDom.BrowserRouter,
	null,
	_react2.default.createElement(MainView, null)
);

_reactDom2.default.render(container, document.getElementById('app-container'));