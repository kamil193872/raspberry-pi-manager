'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _tabs = require('antd/lib/tabs');

var _tabs2 = _interopRequireDefault(_tabs);

var _input = require('antd/lib/input');

var _input2 = _interopRequireDefault(_input);

var _icon = require('antd/lib/icon');

var _icon2 = _interopRequireDefault(_icon);

var _button = require('antd/lib/button');

var _button2 = _interopRequireDefault(_button);

var _moment = require('moment');

var _moment2 = _interopRequireDefault(_moment);

require('antd/lib/tabs/style/css');

require('antd/lib/input/style/css');

require('antd/lib/icon/style/css');

require('antd/lib/button/style/css');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var TabPane = _tabs2.default.TabPane;

function callback(key) {
    console.log(key);
}

var URL_IMG = "https://cdn.iconscout.com/public/images/icon/free/png-48/raspberry-pi-logo-34e5a2f43f2e9f43-48x48.png";

var DeviceListItem = function (_React$Component) {
    _inherits(DeviceListItem, _React$Component);

    function DeviceListItem(props) {
        _classCallCheck(this, DeviceListItem);

        var _this = _possibleConstructorReturn(this, (DeviceListItem.__proto__ || Object.getPrototypeOf(DeviceListItem)).call(this, props));

        _this.state = {
            id: props.data.id,
            name: props.data.name,
            icon: props.data.icon || URL_IMG,
            date: props.data.date,
            extensions: props.data.extensions || [],
            active: false,
            onlineTimerId: null,
            transparentMode: false,
            data: {},
            onlineTime: null,
            response: null
        };
        return _this;
    }

    _createClass(DeviceListItem, [{
        key: 'componentWillUnmount',
        value: function componentWillUnmount() {
            clearInterval(this.state.onlineTimerId);
            this.setState({ onlineTimerId: null });
        }
    }, {
        key: 'componentDidMount',
        value: function componentDidMount() {
            var onlineTimerId = setInterval(this.getOnlineTime.bind(this), 1000);
            this.setState({ onlineTimerId: onlineTimerId });
            this.getOnlineTime(true);
        }
    }, {
        key: 'componentWillReceiveProps',
        value: function componentWillReceiveProps(props) {
            this.setState(props.data);
        }
    }, {
        key: 'selectionChanged',
        value: function selectionChanged(isSelected) {
            this.props.selectionChanged({
                activeDevice: isSelected ? this.state.id : null,
                transparentMode: isSelected
            });
        }
    }, {
        key: 'callClientFunction',
        value: function callClientFunction(extName) {
            console.log(extName);
            this.props.callExternalFunction(this.state.id, extName, this.state.data);
        }
    }, {
        key: 'onInput',
        value: function onInput(fieldName, value) {
            var data = this.state.data;
            data[fieldName] = value;
            this.setState({ data: data });
        }
    }, {
        key: 'render',
        value: function render() {
            var _this2 = this;

            var classes = "device-list-item";

            classes = this.state.active ? classes += " device-list-active" : classes;
            classes = this.state.transparentMode && !this.state.active ? classes += " device-list-transparent" : classes;

            var extensionBadge = !this.state.active && this.state.extensions.length ? _react2.default.createElement(
                'p',
                { className: 'extension-badge' },
                'Extension ',
                _react2.default.createElement(
                    'span',
                    { className: 'badge badge-secondary' },
                    this.state.extensions.length
                )
            ) : null;

            var online = this.state.date && !this.state.active ? _react2.default.createElement(
                'span',
                { className: 'device-list-item-timer' },
                this.state.onlineTime
            ) : null;

            var extensions = this.state.active ? _react2.default.createElement(
                _tabs2.default,
                { onChange: callback },
                this.state.extensions.map(function (ext) {
                    return _react2.default.createElement(
                        TabPane,
                        { tab: ext.name, key: ext.name },
                        _react2.default.createElement(
                            'span',
                            { className: 'device-list-item-desc' },
                            ext.description
                        ),
                        _react2.default.createElement(
                            _input2.default.Group,
                            null,
                            ext.input.map(function (field) {
                                return _react2.default.createElement(_input2.default, { type: 'text',
                                    name: field.name,
                                    key: field.name,
                                    placeholder: field.description,
                                    onChange: function onChange(evt) {
                                        _this2.onInput(field.name, evt.target.value);
                                    }
                                });
                            }),
                            _react2.default.createElement(
                                _button2.default,
                                { type: 'primary', onClick: function onClick() {
                                        _this2.callClientFunction(ext.name);
                                    } },
                                _react2.default.createElement(_icon2.default, { type: 'arrow-right' })
                            ),
                            _react2.default.createElement(
                                'pre',
                                { className: 'device-list-item-response' },
                                ext.response || ""
                            )
                        )
                    );
                }),
                _react2.default.createElement(
                    TabPane,
                    { tab: 'Placeholder 1', key: '2' },
                    'Content of Tab Placeholder 1'
                ),
                _react2.default.createElement(
                    TabPane,
                    { tab: 'Placeholder 2', key: '3' },
                    'Content of Tab Placeholder 2'
                )
            ) : null;

            return _react2.default.createElement(
                'div',
                { tabIndex: '0', className: classes, onFocus: function onFocus() {
                        _this2.selectionChanged(true);
                    } },
                _react2.default.createElement(
                    'button',
                    { className: 'close', onClick: function onClick() {
                            _this2.selectionChanged(false);
                        } },
                    '\xD7'
                ),
                _react2.default.createElement('img', { src: this.state.icon, className: 'device-list-item-icon' }),
                _react2.default.createElement(
                    'span',
                    { className: 'device-list-item-name' },
                    this.state.name
                ),
                !this.state.active ? _react2.default.createElement('hr', null) : null,
                online,
                extensionBadge,
                extensions
            );
        }
    }, {
        key: 'getOnlineTime',
        value: function getOnlineTime(force) {
            if (force === true || !this.state.active) {
                var then = (0, _moment2.default)(this.state.date),
                    now = (0, _moment2.default)(Date.now()),
                    diff = now.diff(then),
                    hours = parseInt(_moment2.default.duration(diff).asHours()),
                    minutes = parseInt(_moment2.default.duration(diff - hours * 3600 * 1000).asMinutes()),
                    seconds = parseInt(_moment2.default.duration(diff - hours * 3600 * 1000 - minutes * 60 * 1000).asSeconds());
                var onlineTime = 'Online ' + hours + 'h : ' + minutes + 'min : ' + seconds + 's';

                this.setState({ onlineTime: onlineTime });
            }
        }
    }]);

    return DeviceListItem;
}(_react2.default.Component);

exports.default = DeviceListItem;