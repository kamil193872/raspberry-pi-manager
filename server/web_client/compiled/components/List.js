"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require("react");

var _react2 = _interopRequireDefault(_react);

var _DeviceListItem = require("./DeviceListItem");

var _DeviceListItem2 = _interopRequireDefault(_DeviceListItem);

var _UserListItem = require("./UserListItem");

var _UserListItem2 = _interopRequireDefault(_UserListItem);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var List = function (_React$Component) {
    _inherits(List, _React$Component);

    function List(props) {
        _classCallCheck(this, List);

        var _this = _possibleConstructorReturn(this, (List.__proto__ || Object.getPrototypeOf(List)).call(this, props));

        _this.state = {
            items: props.items || [],
            type: props.type,
            transparentMode: false,
            activeDevice: null
        };
        return _this;
    }

    _createClass(List, [{
        key: "componentWillReceiveProps",
        value: function componentWillReceiveProps(props) {
            this.setState(props);
        }
    }, {
        key: "selectionChanged",
        value: function selectionChanged(state) {
            this.setState(state);
        }
    }, {
        key: "render",
        value: function render() {
            var _this2 = this;

            return _react2.default.createElement(
                "div",
                { className: "row" },
                this.state.items.map(function (item) {
                    var listItem = null;

                    switch (_this2.state.type) {
                        case "USERS":
                            {
                                listItem = _react2.default.createElement(_UserListItem2.default, { key: item, data: item });
                                break;
                            }
                        case "DEVICES":
                            {
                                item.transparentMode = _this2.state.transparentMode;
                                item.active = _this2.state.activeDevice === item.id;

                                listItem = _react2.default.createElement(_DeviceListItem2.default, {
                                    key: item.id,
                                    data: item,
                                    selectionChanged: function selectionChanged(state) {
                                        _this2.selectionChanged(state);
                                    },
                                    callExternalFunction: _this2.props.callExternalFunction
                                });
                                break;
                            }

                    }

                    return listItem;
                })
            );
        }
    }]);

    return List;
}(_react2.default.Component);

exports.default = List;