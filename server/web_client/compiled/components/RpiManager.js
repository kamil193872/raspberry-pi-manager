'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _List = require('./List');

var _List2 = _interopRequireDefault(_List);

var _SocketIO = require('../api/SocketIO');

var _SocketIO2 = _interopRequireDefault(_SocketIO);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var EVT_CLNT_CLIENT_UPDATE = 'client-update',
    EVT_CLNT_COMMAND_RESPONSE = 'client-cmd-response',
    EVT_CMN_COMMAND = 'command',
    EVT_CLNT_USER_LIST = 'user-list';

var RpiManager = function (_React$Component) {
    _inherits(RpiManager, _React$Component);

    function RpiManager(props) {
        _classCallCheck(this, RpiManager);

        var _this = _possibleConstructorReturn(this, (RpiManager.__proto__ || Object.getPrototypeOf(RpiManager)).call(this, props));

        _this.state = {
            users: [],
            clients: []
        };
        return _this;
    }

    _createClass(RpiManager, [{
        key: 'componentDidMount',
        value: function componentDidMount() {
            var _this2 = this;

            _SocketIO2.default.on(EVT_CMN_COMMAND, function (data) {
                console.log('command: ' + data);
            }).on(EVT_CLNT_COMMAND_RESPONSE, function (_ref) {
                var callerId = _ref.callerId,
                    extName = _ref.extName,
                    data = _ref.data;

                console.log(EVT_CLNT_COMMAND_RESPONSE + " : " + data.toString());
                console.timeEnd(callerId);

                var newClients = Array.from(_this2.state.clients);

                newClients.forEach(function (client) {
                    if (client.id == callerId) {
                        (client.extensions || []).forEach(function (ext) {
                            if (ext.name == extName) ext.response = data;
                        });
                    }
                });

                _this2.setState({ clients: newClients });
                _this2.clearResponse(callerId, extName, 3000);
            }).on(EVT_CLNT_USER_LIST, function (data) {
                //insertListData(data);
                console.log("USER LIST");
                console.log(data);
                _this2.setState({
                    users: data.users,
                    clients: data.clients
                });
            }).on("connect", function () {
                console.log('polaczony, ID: ' + _SocketIO2.default.id);
            }).on(EVT_CLNT_CLIENT_UPDATE, function (data) {
                if (!data || !data.id || !data.name) return;
                console.log(EVT_CLNT_CLIENT_UPDATE);
                console.log(data);
            });
        }
    }, {
        key: 'clearResponse',
        value: function clearResponse(callerId, extName, time) {
            setTimeout(function () {
                var _this3 = this;

                var newClients = Array.from(this.state.clients);

                newClients.forEach(function (client) {
                    if (client.id == callerId) {
                        (client.extensions || []).forEach(function (ext) {
                            if (ext.name == extName) ext.response = "";
                        });
                    }
                    _this3.setState({ clients: newClients });
                });

                this.setState({ clients: newClients });
            }.bind(this), time);
        }
    }, {
        key: 'callExternalFunction',
        value: function callExternalFunction(clientId, extName, value) {
            console.time(clientId);
            console.log('client: ' + clientId + ', function name: ' + extName + ', value: ' + value);
            _SocketIO2.default.emit(EVT_CMN_COMMAND, clientId, extName, value);
        }
    }, {
        key: 'render',
        value: function render() {
            return _react2.default.createElement(
                'div',
                { className: 'row' },
                _react2.default.createElement(
                    'div',
                    { className: 'user-list' },
                    _react2.default.createElement(
                        'h2',
                        null,
                        'Users'
                    ),
                    _react2.default.createElement(_List2.default, { type: 'USERS', items: this.state.users })
                ),
                _react2.default.createElement(
                    'div',
                    { className: 'device-list' },
                    _react2.default.createElement(
                        'h2',
                        null,
                        'Devices'
                    ),
                    _react2.default.createElement(_List2.default, { type: 'DEVICES', items: this.state.clients, callExternalFunction: this.callExternalFunction })
                )
            );
        }
    }]);

    return RpiManager;
}(_react2.default.Component);

exports.default = RpiManager;