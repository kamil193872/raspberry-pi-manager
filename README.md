# README #

Raspberry Pi Manager allows to connect and read data from many RPIs.
Tech stack: Node.js, React, Jest, Socket.io, webpack, babel, 

Each *Raspberry Pi** may be configured to allow user to call remotelly some functions named Extensions. Extension may be simple function or module that returns data to caller - user using lynx2k.usermd.net site. 

Example: function that reads data from GPIO termometer and returns it to user.

live - http://lynx2k.usermd.net

*it doesn't have to be Raspberry Pi, every device that can handle Node.js and provides internet connection is suitable

## Structure
Project consists of 3 subprojects:

*  server app: server
*  rpi client app: rpi-client
*  webclient app: server/public

## Create local instance of server RPI Manager

-  Clone repo
-  Install dependencies
```
cd server
npm install
```
-  Run server
```
node server.js
```
-   Install RPI-Client dependencies
```
cd ..\rpi-client\
npm install
```
-   Run client instance with sample Extension function
 
```
node client.js
```

-  Go to http://localhost:3000


## Create RPI-Client Extensions
See sample Extensions in rpi-client\extensions


## Future
Things to implement:

1.  Chaining clients to create interactions between them, example: if rpi_1 measure temperature heigher than X, then call Extension function on rpi_2
2.  Authentication (JTW)
3.  UI improvements
4.  Suggest something!

