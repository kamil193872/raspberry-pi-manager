const io = require('socket.io-client');
const fs = require('fs');
const path = require('path');
const extensions = require('./components/ExtensionLoader/extLoader')();

//let usersNS = io("http://lynx2k.usermd.net/client");
let usersNS = io("http://localhost:3000/client");
const confFile = fs.readFileSync(path.join(__dirname,'CONFIG.json')),
	CONFIG = JSON.parse(confFile),
    clientName = CONFIG.name,
	clientIcon = CONFIG.icon;
const EVT_CMN_COMMAND = 'command',
    EVT_CLNT_CLIENT_UPDATE = 'client-update',
    EVT_CLNT_COMMAND_RESPONSE = 'client-cmd-response';
let MY_ID = null;

usersNS.on(EVT_CMN_COMMAND, async ({callerId, extName, data})=>{
    console.log(`command: ${data}`);

    let extParams = extensions.find(ext => ext.name === extName),
        extModule = (!!extParams) ? require(extParams.module) : null,
        response = null;
    if (!!extModule) {
        response = extModule.call(null, data);
    }
    console.log('response :' + response);
    //let response =
    usersNS.emit(EVT_CLNT_COMMAND_RESPONSE, {callerId: MY_ID, destinationId: callerId, data: response, extName} );
})
.on("connect",()=>{
    console.log(`polaczony, ID: ${usersNS.id}`);
    console.log(extensions);
    MY_ID = usersNS.id;
    let ext = extensions.map( ext => {
        return {name: ext.name,
            input: ext.input,
            output: ext.output,
            description: (ext.description || "")
        }
    });
    usersNS.emit(EVT_CLNT_CLIENT_UPDATE, {"id": MY_ID, "name": clientName, "icon": clientIcon, "extensions": ext});
})

console.log('Client started...');
//console.log("extensions: " + extensions.map(item=>item.name).join(', '));