const fs = require('fs');
const util = require('util');
const path = require('path');

const extPath = path.join(__dirname, '..', '..', 'extensions'),
	CONFIG_FILE_NAME = "config.json";

	const isConfigFileValid = (file, path) => {
		let valid = !!file && !!path && "name" in file && "input" in file && "output" in file && "fileName" in file && "functionName" in file;

		return valid && fs.existsSync(path);
	}
module.exports = () => {
	
	console.log(`SEARCHING FOR EXTENSIONS:  ${extPath} \n`);
	let extensions = [],
		readDir = util.promisify(fs.readdir);

	let files = fs.readdirSync(extPath);

	files.forEach(function(extension) {
		
		let configFile = undefined;
		const extensionPath = path.join(extPath, extension, CONFIG_FILE_NAME);

		try{
			configFile = fs.readFileSync(extensionPath, "utf-8");
		}catch(err){
			console.log(err);
		}

		configFile = !!configFile ? JSON.parse(configFile) : null;
		let modulePath = path.join(extPath, extension, configFile["fileName"]);

		if(isConfigFileValid(configFile, modulePath)){
			configFile["module"] = modulePath;
			extensions.push(configFile);
		}

	});

	return extensions;
	
}
